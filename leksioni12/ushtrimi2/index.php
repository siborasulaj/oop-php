<?php

include_once "Perdorues.php";
include_once "SessionX.php";

$session = new SessionX();

if (isset($_POST["Register"])){
    $emri = $_POST['emri'];
    $password = $_POST['password'];
    $sessionArray = $session->kaloTeDhena($emri, $password);
    $perdorues = new Perdorues($sessionArray['emri'], $sessionArray['password']);
    $perdorues->insert();
}

if (isset($_POST["Ndrysho"])){
    $emri = $_POST['emri'];
    $password = $_POST['newPassword'];
    $sessionArray = $session->kaloTeDhena($emri, $password);
    $perdorues = new Perdorues($sessionArray['emri'], $sessionArray['password']);
    $perdorues->setPassword($sessionArray['emri'], $sessionArray['password']);
}
?>

<html>
<body>
<form method="post">
    <input type="submit" name="NdryshoPassword" value="Ndrysho Password"><br>
    <input type="submit" name="LogOut" value="Log Out">
</form>
</body>
</html>

<?php
if (isset($_POST["NdryshoPassword"])){
    header("Location: NdryshoPassword.php");
}
if (isset($_POST["LogOut"])){
    session_destroy();
    header("Location: logIn.php");
}