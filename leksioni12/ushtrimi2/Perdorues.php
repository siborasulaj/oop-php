<?php

include_once "DBConnect.php";

class Perdorues{
    private $emri;
    private $password;
    private $instance;

    public function __construct(string $emri, string $password)
    {
        $this->emri = $emri;
        $this->password = $password;
    }

    /**
     * @param string $emri
     */
    public function setEmri(string $emri)
    {
        $this->emri = $emri;
    }

    /**
     * @return string
     */
    public function getEmri(): string
    {
        return $this->emri;
    }

    /**
     * @param string $emri
     * @param string $password
     */
    public function setPassword(string $emri, string $password)
    {
        try{
            $this->password = $password;

            $this->instance = DBConnect::getInstance();

            $sql = "UPDATE perdorues SET password=:password
                WHERE emri=:emri";
            $query = $this->instance->getConnection()->prepare($sql);
            $query->bindParam(':emri',$this->emri, PDO::PARAM_STR);
            $query->bindParam(':password',$this->password, PDO::PARAM_STR);

            $this->emri = $emri;
            $this->password = $password;
            $query->execute();
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }

    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function insert(){
        $this->instance = DBConnect::getInstance();

        $sql = "INSERT INTO perdorues (emri, password)
                VALUES(:emri,:password)";
        $query = $this->instance->getConnection()->prepare($sql);
        $query->bindParam(':emri',$this->emri, PDO::PARAM_STR);
        $query->bindParam(':password',$this->password, PDO::PARAM_STR);

        $query->execute();
    }

    public function checkPerdorues(){
        $this->instance = DBConnect::getInstance();

        $sql = "SELECT * FROM perdorues WHERE emri = :emri";
        $query = $this->instance->getConnection()->prepare($sql) ;
        $query -> bindParam(':emri', $this->emri);

        $query->execute();

        $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

        if ($rezultati[0]["Emri"] == $this->emri && $rezultati[0]["Password"] == $this->password){
            return true;
        }
        else{
            return false;
        }
    }
}