<html>
<body>
<form action="logIn.php" method="post">
    <label for="emri">Username: </label>
    <input type="text" name="emri"><br>
    <label for="password">Password: </label>
    <input type="password" name="password"><br>
    <input type="submit" name="logIn" value="Log In">
</form>
<p>Don't have an account?<a href="Register.php">Register</a></p>
</body>
</html>

<?php
include_once "Perdorues.php";
include_once "SessionX.php";

$session = new SessionX();

if (isset($_POST["logIn"])){
    $emri = $_POST['emri'];
    $password = $_POST['password'];
    $sessionArray = $session->kaloTeDhena($emri, $password);
    $perdorues = new Perdorues($sessionArray['emri'], $sessionArray['password']);

    if ($perdorues->checkPerdorues()){
        header("Location: index.php");
    }
    else{
        echo "Te dhena te gabuara!";
    }
}