<?php

include_once "Produkt.php";

class ProduktFactory{
    public function getProduktData(string $emri, float $cmimi){
        return new Produkt($emri, $cmimi);
    }
}