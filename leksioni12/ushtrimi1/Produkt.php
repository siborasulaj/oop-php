<?php

class Produkt{
    private $emri;
    private $cmimi;

    public function __construct(string $emri, float $cmimi){
        $this->emri = $emri;
        $this->cmimi = $cmimi;
    }

    /**
     * @param string $emri
     */
    public function setEmri(string $emri)
    {
        $this->emri = $emri;
    }

    /**
     * @return string
     */
    public function getEmri(): string
    {
        return $this->emri;
    }

    /**
     * @param float $cmimi
     */
    public function setCmimi(float $cmimi)
    {
        $this->cmimi = $cmimi;
    }

    /**
     * @return float
     */
    public function getCmimi(): float
    {
        return $this->cmimi;
    }
}