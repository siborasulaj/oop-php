<?php

include_once "Artikull.php";
include_once "Autor.php";

$artikuj = array(
    array(
        "artikulli" => "Artikull 1",
        "autori" => ["Sibora", "Sulaj", 20]
    ),
    array(
        "artikulli" => "Artikull 2",
        "autori" => ["Igli", "Sulaj", 29]
    ),
    array(
        "artikulli" => "Artikull 3",
        "autori" => ["Olisian", "Sulaj", 30]
    )
);

foreach ($artikuj as $artikull){
    $autor = new Autor($artikull["autori"][0], $artikull["autori"][1], $artikull["autori"][2]);
    $artikull = new Artikull($artikull["artikulli"], $autor);

    echo "Artikulli ".$artikull->getTitull()." nga autori ".$artikull->getAutor()->getEmer()." ".$artikull->getAutor()->getMbiemer()."<br>";
}