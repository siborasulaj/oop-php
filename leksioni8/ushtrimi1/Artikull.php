<?php

include_once "Autor.php";

class Artikull{
    private $titull;
    private $autor;

    public function __construct(string $titull, Autor $autor){
        $this->titull = $titull;
        $this->autor = $autor;
    }

    /**
     * @param string $titull
     */
    public function setTitull(string $titull)
    {
        $this->titull = $titull;
    }

    /**
     * @return string
     */
    public function getTitull(): string
    {
        return $this->titull;
    }

    /**
     * @param Autor $autor
     */
    public function setAutor(Autor $autor)
    {
        $this->autor = $autor;
    }

    /**
     * @return Autor
     */
    public function getAutor(): Autor
    {
        return $this->autor;
    }
}
