<?php

class Autor{

    private $emer;
    private $mbiemer;
    private $mosha;

    public function __construct(string $emer, string $mbiemer, int $mosha){
        $this->emer = $emer;
        $this->mbiemer = $mbiemer;
        $this->mosha = $mosha;
    }

    /**
     * @param string $emer
     */
    public function setEmer(string $emer)
    {
        $this->emer = $emer;
    }

    /**
     * @return string
     */
    public function getEmer(): string
    {
        return $this->emer;
    }

    /**
     * @param string $mbiemer
     */
    public function setMbiemer(string $mbiemer)
    {
        $this->mbiemer = $mbiemer;
    }

    /**
     * @return string
     */
    public function getMbiemer(): string
    {
        return $this->mbiemer;
    }

    /**
     * @param int $mosha
     */
    public function setMosha(int $mosha)
    {
        $this->mosha = $mosha;
    }

    /**
     * @return int
     */
    public function getMosha(): int
    {
        return $this->mosha;
    }
}