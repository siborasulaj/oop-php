<?php

class Pedagog{
    private $emri;
    private $mbiemri;
    private $lenda;

    public function __construct(string $emri, string $mbiemri, string $lenda){
        $this->emri = $emri;
        $this->mbiemri = $mbiemri;
        $this->lenda = $lenda;
    }

    /**
     * @param string $emri
     */
    public function setEmri(string $emri)
    {
        $this->emri = $emri;
    }

    /**
     * @return string
     */
    public function getEmri(): string
    {
        return $this->emri;
    }

    /**
     * @param string $mbiemri
     */
    public function setMbiemri(string $mbiemri)
    {
        $this->mbiemri = $mbiemri;
    }

    /**
     * @return string
     */
    public function getMbiemri(): string
    {
        return $this->mbiemri;
    }

    /**
     * @param string $lenda
     */
    public function setLenda(string $lenda)
    {
        $this->lenda = $lenda;
    }

    /**
     * @return string
     */
    public function getLenda(): string
    {
        return $this->lenda;
    }
}