<?php

include_once "Pedagog.php";
include_once "Student.php";

class Fakultet{
    private $emri;

    public function __construct(string $emri){
        $this->emri = $emri;
    }

    /**
     * @param string $emri
     */
    public function setEmri(string $emri)
    {
        $this->emri = $emri;
    }

    /**
     * @return string
     */
    public function getEmri(): string
    {
        return $this->emri;
    }
}