<?php

include_once "Student.php";
include_once "Pedagog.php";
include_once "Fakultet.php";

$pedagog = new Pedagog("Dejvid", "Haxhiu","Programim OOP");

$fakultet = new Fakultet("FSHN");

$student = new Student("Sibora", "Sulaj", "Informatike", $fakultet, $pedagog);

echo "Studenti ".$student->getEmri()." ".$student->getMbiemri().
    " studion ne fakultetin ".$student->getFakultet()->getEmri()." ne degen ".$student->getDega().
    " dhe ka pedagog te lendes ".$student->getPedagog()->getLenda()." pedagogun ".$student->getPedagog()->getEmri()." ".$student->getPedagog()->getMbiemri();