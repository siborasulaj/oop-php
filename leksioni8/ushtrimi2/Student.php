<?php

class Student{
    private $emri;
    private $mbiemri;
    private $dega;
    private $fakultet;
    private $pedagog;

    public function __construct(string $emri, string $mbiemri, string $dega, Fakultet $fakultet, Pedagog $pedagog){
        $this->emri = $emri;
        $this->mbiemri = $mbiemri;
        $this->dega = $dega;
        $this->fakultet = $fakultet;
        $this->pedagog = $pedagog;
    }

    /**
     * @param string $emri
     */
    public function setEmri(string $emri)
    {
        $this->emri = $emri;
    }

    /**
     * @return string
     */
    public function getEmri(): string
    {
        return $this->emri;
    }

    /**
     * @param string $mbiemri
     */
    public function setMbiemri(string $mbiemri)
    {
        $this->mbiemri = $mbiemri;
    }

    /**
     * @return string
     */
    public function getMbiemri(): string
    {
        return $this->mbiemri;
    }

    /**
     * @param string $dega
     */
    public function setDega(string $dega)
    {
        $this->dega = $dega;
    }

    /**
     * @return string
     */
    public function getDega(): string
    {
        return $this->dega;
    }

    /**
     * @param Fakultet $fakultet
     */
    public function setFakultet(Fakultet $fakultet)
    {
        $this->fakultet = $fakultet;
    }

    /**
     * @return Fakultet
     */
    public function getFakultet(): Fakultet
    {
        return $this->fakultet;
    }

    /**
     * @param Pedagog $pedagog
     */
    public function setPedagog(Pedagog $pedagog)
    {
        $this->pedagog = $pedagog;
    }

    /**
     * @return Pedagog
     */
    public function getPedagog(): Pedagog
    {
        return $this->pedagog;
    }
}