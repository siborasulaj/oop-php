<?php
include "Punetor.php";
include "SuperPunetor.php";

$punetor1 = new Punetor("Sibora", "Sulaj", 1000, 40);
$punetor2 = new Punetor("Igli", "Sulaj", 1100, 40);
$punetor3 = new Punetor("Olisian", "Sulaj", 900, 40);
$punetor4 = new Punetor("Elena", "Sulaj", 950,40);
$punetor5 = new SuperPunetor(30,5,"Harry", "Potter", 900, 40);
$punetor6 = new SuperPunetor(50,10, "Ronald", "Weasely", 970, 40);

$punetore = array($punetor1, $punetor2, $punetor3, $punetor4, $punetor5, $punetor6);

$shuma = 0;
foreach ($punetore as $value){
    $shuma += $value->llogaritPagen();
}

echo "Jane krijuar ".$punetor1->getCount()." punetore<br><br>";

?>
<html>
<body>
<table border="1">
    <tr>
        <th>Emri</th>
        <th>Mbiemri</th>
        <th>Paga per ore</th>
        <th>Numri i oreve</th>
        <th>Totali</th>
    </tr>
    <?php
    foreach ($punetore as $value):
    ?>
    <tr>
        <td><?php echo $value->getEmer()?></td>
        <td><?php echo $value->getMbiemer()?></td>
        <td><?php echo $value->getPagaPerOre()?></td>
        <td><?php echo $value->getNrIOreve()?></td>
        <td><?php echo $value->llogaritPagen()?></td>
    </tr>
    <?php endforeach;?>
    <tr>
        <td><b>Totali</b></td>
        <td></td>
        <td></td>
        <td></td>
        <td><b><?php echo $shuma?></b></td>
    </tr>
</table>
</body>
</html>
