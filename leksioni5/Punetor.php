<?php
class Punetor{
    private $emer;
    private $mbiemer;
    private $pagaPerOre;
    private $nrIOreve;
    private static $count = 0;

    public function __construct($emer, $mbiemer, $pagaPerOre,$nrIOreve){
        $this->emer = $emer;
        $this->mbiemer = $mbiemer;
        $this->pagaPerOre = $pagaPerOre;
        $this->nrIOreve = $nrIOreve;
        Punetor::$count++;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return Punetor::$count;
    }

    /**
     * @return mixed
     */
    public function getEmer()
    {
        return $this->emer;
    }

    /**
     * @return mixed
     */
    public function getMbiemer()
    {
        return $this->mbiemer;
    }

    /**
     * @return mixed
     */
    public function getPagaPerOre()
    {
        return $this->pagaPerOre;
    }

    /**
     * @return mixed
     */
    public function getNrIOreve()
    {
        return $this->nrIOreve;
    }

    /**
     * @param mixed $emer
     */
    public function setEmer($emer)
    {
        $this->emer = $emer;
    }

    /**
     * @param mixed $mbiemer
     */
    public function setMbiemer($mbiemer)
    {
        $this->mbiemer = $mbiemer;
    }

    /**
     * @param mixed $pagaPerOre
     */
    public function setPagaPerOre($pagaPerOre)
    {
        $this->pagaPerOre = $pagaPerOre;
    }

    /**
     * @param mixed $nrIOreve
     */
    public function setNrIOreve($nrIOreve)
    {
        $this->nrIOreve = $nrIOreve;
    }
    public function llogaritPagen(){
        return $this->pagaPerOre*$this->nrIOreve;
    }
}