<?php

class SuperPunetor extends Punetor
{
    private $perqindja;
    private $oreShtese;

    public function __construct($perqindja, $oreShtese, $emer, $mbiemer, $pagaPerOre, $nrIOreve)
    {
        $this->perqindja = $perqindja;
        $this->oreShtese = $oreShtese;
        parent::__construct($emer, $mbiemer, $pagaPerOre, $nrIOreve);
    }

    /**
     * @param mixed $oreShtese
     */
    public function setOreShtese($oreShtese)
    {
        $this->oreShtese = $oreShtese;
    }

    /**
     * @return mixed
     */
    public function getOreShtese()
    {
        return $this->oreShtese;
    }

    /**
     * @param mixed $perqindja
     */
    public function setPerqindja($perqindja)
    {
        $this->perqindja = $perqindja;
    }

    /**
     * @return mixed
     */
    public function getPerqindja()
    {
        return $this->perqindja;
    }
    public function llogaritPagen()
    {
        return parent::llogaritPagen()+($this->perqindja/100)*$this->getPagaPerOre()*$this->oreShtese;
    }
}