<?php
class Calculator{
    private $numri1;
    private $numri2;
    private $veprimi;

    public function __construct($numri1, $numri2, $veprimi){
        $this->numri1 = $numri1;
        $this->numri2 = $numri2;
        $this->veprimi = $veprimi;
    }

    /**
     * @return mixed
     */
    public function getNumri1()
    {
        return $this->numri1;
    }

    /**
     * @return mixed
     */
    public function getNumri2()
    {
        return $this->numri2;
    }

    /**
     * @return mixed
     */
    public function getVeprimi()
    {
        return $this->veprimi;
    }

    /**
     * @param mixed $numri1
     */
    public function setNumri1($numri1)
    {
        $this->numri1 = $numri1;
    }

    /**
     * @param mixed $numri2
     */
    public function setNumri2($numri2)
    {
        $this->numri2 = $numri2;
    }

    /**
     * @param mixed $veprimi
     */
    public function setVeprimi($veprimi)
    {
        $this->veprimi = $veprimi;
    }

    public function kryejVeprim(){
        $result = "";
        $vlera = 0;
        switch ($this->veprimi){
            case '+':
                $vlera = $this->numri1+$this->numri2;
                $result = "Shuma e numrave ".$this->numri1." dhe ".$this->numri2." eshte ".$vlera;
                break;
            case '-':
                $vlera = $this->numri1-$this->numri2;
                $result = "Ndryshesa e numrave ".$this->numri1." dhe ".$this->numri2." eshte ".$vlera;
                break;
            case '*':
                $vlera = $this->numri1*$this->numri2;
                $result = "Prodhimi i numrave ".$this->numri1." dhe ".$this->numri2." eshte ".$vlera;
                break;
            case '/':
                if($this->numri2==0){
                    $result = "Nuk lejohet pjesetimi me zero!";
                }
                else{
                    $vlera = $this->numri1/$this->numri2;
                    $result = "Heresi i numrave ".$this->numri1." dhe ".$this->numri2." eshte ".$vlera;
                }
                break;
            default:
                echo "Gabim!";
        }
        return $result;
    }
    public function eshteNumer(){
        if (!is_numeric($this->numri1) || !is_numeric($this->numri2))
            return false;
        else
            return true;
    }
}