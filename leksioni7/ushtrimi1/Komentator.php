<?php

include_once "Perdorues.php";

class Komentator implements Perdorues{
    private $emri;
    private $gjinia;

    public function __construct($emri, $gjinia){
        $this->emri = $emri;
        $this->gjinia = $gjinia;
    }
    public function setEmri(string $emri)
    {
        $this->emri = $emri;
    }
    public function getEmri(): string
    {
        return $this->emri;
    }
    public function setGjinia(string $gjinia)
    {
        $this->gjinia = $gjinia;
    }
    public function getGjinia(): string
    {
        return $this->gjinia;
    }
    public function setHonorifics(){
        if (is_string($this->gjinia)){
            if ($this->gjinia == "Femer"){
                return "Mrs. ".$this->emri;
            }
            elseif ($this->gjinia == "Mashkull"){
                return "Mr. ".$this->emri;
            }
        }
        else{
            return "Ju lutem vendosni gjinine te tipit string!";
        }
    }
}