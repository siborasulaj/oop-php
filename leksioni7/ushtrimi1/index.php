<?php

include_once "Komentator.php";

$komentator1 = new Komentator("Sibora Sulaj", "Femer");
$komentator2 = new Komentator("Igli Sulaj", "Mashkull");
$komentator3 = new Komentator("Olisian Sulaj", 2);

echo $komentator1->setHonorifics()."<br>";
echo $komentator2->setHonorifics()."<br>";
echo $komentator3->setHonorifics();