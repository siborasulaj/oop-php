<?php

interface Perdorues{
    function setEmri(string $emri);
    function getEmri():string;
    function setGjinia(string $gjinia);
    function getGjinia():string;
}