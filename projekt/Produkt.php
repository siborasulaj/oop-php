<?php

include_once "Kategori.php";
include_once "DBConnect.php";

//namespace Produkt;
//use Produkt\Kategori;

class Produkt{
    private static $instance;
    private $id;
    private $emer;
    private $cmim;
    private $kategori_id;

    /**
     * Produkt constructor.
     * @param string $id
     * @param string $emer
     * @param float $cmim
     * @param Kategori $kategori
     */
    public function __construct(string $emer, int $cmim, Kategori $kategori)
    {
        $this->emer = $emer;
        $this->cmim = $cmim;
        $this->kategori_id = $kategori->getId();
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "SELECT * FROM produkt WHERE Emri = :emri AND Cmimi = :cmimi";
            $query = self::$instance->getConnection()->prepare($sql) ;
            $query -> bindParam(':emri', $this->emer, PDO::PARAM_STR);
            $query -> bindParam(':cmimi', $this->cmim, PDO::PARAM_INT);

            $query->execute();

            $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

            $this->id = $rezultati[0]["id"];
            return $this->id;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @param string $emer
     */
    public function setEmer(string $emer)
    {
        $this->emer = $emer;
    }

    /**
     * @return string
     */
    public function getEmer(): string
    {
        return $this->emer;
    }

    /**
     * @param float $cmim
     */
    public function setCmim(float $cmim)
    {
        $this->cmim = $cmim;
    }

    /**
     * @return float
     */
    public function getCmim(): float
    {
        return $this->cmim;
    }

    /**
     * @param Kategori $kategori
     */
    public function setKategori(Kategori $kategori)
    {
        $this->kategori_id = $kategori->getId();
    }

    /**
     * @return int
     */
    public function getKategori_id(): int
    {
        return $this->kategori_id;
    }
    /**
     * @param Produkt $produkt
     */
    public function shtoProdukt(Produkt $produkt){
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "INSERT INTO produkt (Kategori_id, Emri, Cmimi)
                VALUES(:kategori_id,:emri, :cmimi)";
            $query = self::$instance->getConnection()->prepare($sql);
            $query->bindParam(':kategori_id',$produkt_kategori_id, PDO::PARAM_INT);
            $query->bindParam(':emri',$produkt_emri, PDO::PARAM_STR);
            $query->bindParam(':cmimi',$produkt_cmimi, PDO::PARAM_INT);

            $produkt_kategori_id = $produkt->getKategori_id();
            $produkt_emri = $produkt->getEmer();
            $produkt_cmimi = $produkt->getCmim();

            $query->execute();

            $lastInsertId = self::$instance->getConnection()->lastInsertId();
            if($lastInsertId>0)
            {
                return true;
            }
            else
                return false;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @param string $emerProdukti
     */
    public static function fshiProdukt(string $emerProdukti){
        try {
            $produkte = Produkt::gjejProdukt($emerProdukti);
            self::$instance = DBConnect::getInstance();

            foreach ($produkte as $produkt){
                $sql = "DELETE FROM produkt WHERE id = :id";
                $query = self::$instance->getConnection()->prepare($sql);
                $query->bindParam(':id', $id_produkti, PDO::PARAM_INT);

                $id_produkti = $produkt["id"];

                $query->execute();

                if($query -> rowCount() > 0)
                {
                    $count = $query -> rowCount() ;
                    return true;
                }
                else
                    return false;
            }
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @param string $emerProdukti
     * @param string $fusha_update
     * @param $vlera_update
     */
    public static function modifikoCmimProdukti(string $emerProdukti, $vlera_update){
        try {
            $produkte = Produkt::gjejProdukt($emerProdukti);
            self::$instance = DBConnect::getInstance();

            foreach ($produkte as $produkt){
                $sql = "UPDATE produkt SET Cmimi = :vlera_update
                WHERE id = :id";
                $query = self::$instance->getConnection()->prepare($sql);
                $query->bindParam(':vlera_update',$vlera_update);
                $query->bindParam(':id',$produkt_id, PDO::PARAM_INT);

                $produkt_id = $produkt["id"];

                $query->execute();

                if($query -> rowCount() > 0)
                {
                    $count = $query -> rowCount() ;
                    return true;
                }
                else
                    return false;
            }
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @param string $emerProdukti
     * @param Kategori $kategori
     */
    public static function ndryshoKategoriProduktit(string $emerProdukti, Kategori $kategori){
        try {
            $produkte = Produkt::gjejProdukt($emerProdukti);
            self::$instance = DBConnect::getInstance();
            $kategorite = Kategori::kerkoKategori($kategori->getEmer());
            if (empty($kategorite)){
                return false;
            }

            foreach ($produkte as $produkt){

                $sql = "UPDATE produkt SET Kategori_id = :kategori_id
                WHERE id = :id";
                $query = self::$instance->getConnection()->prepare($sql);
                $query->bindParam(':kategori_id',$kategori_id, PDO::PARAM_STR);
                $query->bindParam(':id',$produkt_id, PDO::PARAM_INT);

                $kategori_id = $kategori->getId();
                $produkt_id = $produkt["id"];

                $query->execute();

                if($query -> rowCount() > 0)
                {
                    $count = $query -> rowCount() ;
                    return true;
                }
                else
                    return false;
            }
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }
    /**
     * @param string $emerProdukti
     * @return array
     */
    public static function gjejProdukt(string $emerProdukti){
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "SELECT * FROM produkt WHERE Emri = :emri";
            $query = self::$instance->getConnection()->prepare($sql) ;
            $query -> bindParam(':emri', $emer, PDO::PARAM_STR);

            $emer = $emerProdukti;

            $query->execute();

            $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

            return $rezultati;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @param Kategori $kategori
     * @return array
     */
    public static function kerkoProduktSipasKategorise(Kategori $kategori){
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "SELECT * FROM produkt WHERE Kategori_id = :kategori_id";
            $query = self::$instance->getConnection()->prepare($sql) ;
            $query -> bindParam(':kategori_id', $kategori_id);

            $kategori_id = $kategori->getId();

            $query->execute();

            $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

            return $rezultati;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }
}