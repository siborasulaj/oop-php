<html>
<head>
    <title>
        Admin
    </title>
</head>
<body>
<h3>Modifiko kategorite</h3>
<ol>
    <li>Shto kategori<br>
        <form action="AdminController.php" method="post">
            <label for="shtimK">Emri i kategorise</label>
            <input type="text" name="shtimK">
            <input type="submit" name="shtoK" value="Shto">
        </form></li>
    <li>Ndrysho emrin e kategorise<br>
        <form action="AdminController.php" method="post">
            <label for="kategoriV">Emri aktual</label>
            <input type="text" name="kategoriV">
            <label for="kategoriR">Emri i ri</label>
            <input type="text" name="kategoriR">
            <input type="submit" name="ndryshoK" value="Ndrysho">
        </form>
    </li>
    <li>Hiq kategori<br>
        <form action="AdminController.php" method="post">
            <label for="hiqK">Emri i kategorise</label>
            <input type="text" name="hiqK">
            <input type="submit" name="hiqKt" value="Hiq">
        </form>
    </li>
</ol>
<h3>Modifiko produktet</h3>
<ol>
    <li>Shto produkt<br>
        <form action="AdminController.php" method="post">
            <label for="emri">Emri i produktit</label>
            <input type="text" name="emri">
            <label for="cmimi">Cmimi i produktit</label>
            <input type="number" name="cmimi">
            <label for="kategori">Kategoria e produktit</label>
            <input type="text" name="kategori">
            <input type="submit" name="shtoP" value="Shto">
        </form></li>
    <li>Ndrysho te dhenat e produktit<br>
        <form action="AdminController.php" method="post">
            <label for="emriP">Emri i produktit</label>
            <input type="text" name="emriP">
            <label for="vlera">Cmimi i ri</label>
            <input type="number" name="vlera">
            <input type="submit" name="ndryshoP" value="Ndrysho">
        </form>
    </li>
    <li>Hiq Produkt<br>
        <form action="AdminController.php" method="post">
            <label for="hiqP">Emri i produktit</label>
            <input type="text" name="hiqP">
            <input type="submit" name="hiqPr" value="Hiq">
        </form>
    </li>
    <li>Ndrysho kategorine e produktit<br>
        <form action="AdminController.php" method="post">
            <label for="emriPr">Emri i produktit</label>
            <input type="text" name="emriPr">
            <label for="kategoriRe">Kategoria e re</label>
            <input type="text" name="kategoriRe">
            <input type="submit" name="ndryshoKP" value="Ndrysho">
        </form>
    </li>
</ol>
<h3>Shiko te dhenat e klienteve</h3>
<form action="AdminController.php" method="post">
    <input type="submit" name="shiko" value="Shiko"><br>
</form>
<form action="AdminController.php" method="post">
    <input type="submit" name="LogOut" value="Log Out">
</form>
</body>
</html>
