<?php
include_once "Klient.php";
include_once "Kategori.php";
include_once "Produkt.php";

session_start();
$perdorues = $_SESSION["perdoruesId"];

if (isset($_POST["kerko"])){
    $emer_produkti = $_POST["produkt"];
    $produkte = Produkt::gjejProdukt($emer_produkti);
    if (empty($produkte)){
        echo "Ky produkt nuk ekziston!";
    }
    else{
        ?>
        <html>
        <body>
        <table border="1">
            <tr>
                <th>
                    Emri
                </th>
                <th>
                    Mbiemri
                </th>
            </tr>
            <?php
            foreach ($produkte as $produkt):
                ?>
                <tr>
                    <td><?php echo $produkt["Emri"] ?></td>
                    <td><?php echo $produkt["Cmimi"] ?></td>
                </tr>
            <?php
            endforeach;
            ?>
        </table>
        </body>
        </html>
        <?php
    }
}
if (isset($_POST["shfaq"])){
    $kategori = new Kategori($_POST["kategori"]);
    $produkte = Produkt::kerkoProduktSipasKategorise($kategori);
    if (empty($produkte)){
        echo "Nuk ka asnje produkt te rregjistruar ne kete kategori!";
    }
    else{
        ?>
        <html>
        <body>
        <table border="1">
            <tr>
                <th>
                    Emri
                </th>
                <th>
                    Mbiemri
                </th>
            </tr>
            <?php
            foreach ($produkte as $produkt):
                ?>
                <tr>
                    <td><?php echo $produkt["Emri"] ?></td>
                    <td><?php echo $produkt["Cmimi"] ?></td>
                </tr>
            <?php
            endforeach;
            ?>
        </table>
        </body>
        </html>
        <?php
    }
}
if (isset($_POST["modifiko"])){
    $emer = $_POST["emri"];
    $mbiemer = $_POST["mbiemri"];
    $email = $_POST["email"];
    $password = $_POST["password"];
    $mosha = $_POST["mosha"];

    $status = Klient::modifikoTeDhena($perdorues, $emer, $mbiemer, $email, $password, $mosha);
    if ($status){
        echo "Te dhenat u perditesuan me sukses!";
    }
    else
        echo "Ndodhi nje gabim ne perditesimin e te dhenave";
}
if (isset($_POST["LogOut"])){
    session_unset();
    header("Location: LogIn.php");
}