<?php
include_once "Kategori.php";
include_once "Produkt.php";
?>

<html>
<head>
    <title>
        Klient
    </title>
</head>
<body>
<h3>Kerko produkt</h3><br>
<form action="KlientController.php" method="post">
    <input type="search" name="produkt">
    <input type="submit" name="kerko" value="Kerko"><br>
</form>
<h3>Shiko listen e kategorive dhe shiko produktet sipas kategorive</h3>
<form action="KlientController.php" method="post">
    <select name="kategori">
<?php
        $kategorite = Kategori::shikoKategorite();
        foreach ($kategorite as $value):
    ?>
    <option>
        <?php echo $value["Emer"] ?>
    </option>
    <?php
        endforeach; ?>
    </select>
    <input type="submit" name="shfaq" value="Shfaq"><br>
</form>
<h3>Modifiko te dhenat e tua</h3>
<form method="post" action="KlientController.php">
    <label for="emri">Emer: </label>
    <input type="text" name="emri"><br>
    <label for="mbiemri">Mbiemer: </label>
    <input type="text" name="mbiemri"><br>
    <label for="email">Email: </label>
    <input type="email" name="email"><br>
    <label for="password">Fjalekalimi: </label>
    <input type="password" name="password"><br>
    <label for="mosha">Mosha: </label>
    <input type="number" name="mosha"><br>
    <input type="submit" name="modifiko" value="Modifiko">
</form>
<form action="KlientController.php" method="post">
    <input type="submit" name="LogOut" value="Log Out">
</form>
</body>
</html>
