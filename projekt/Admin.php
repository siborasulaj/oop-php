<?php
include_once "Perdorues.php";
include_once "DBConnect.php";
include_once "Produkt.php";
include_once "Kategori.php";

//use Perdorues\Perdorues;
//use Produkt\Kategori;
//use Produkt\Produkt;

class Admin extends Perdorues {
    private static $instance;
    private $id;
    private $emer;
    private $mbiemer;
    private $email;
    private $fjalekalim;
    private $tipi = "Admin";
    private $mosha;
    private $gjinia;

    /**
     * Admin constructor.
     * @param $id
     * @param $emer
     * @param $mbiemer
     * @param $email
     * @param $fjalekalim
     */
    public function __construct($emer, $mbiemer, $email, $fjalekalim, $mosha, $gjinia)
    {
        $this->emer = $emer;
        $this->mbiemer = $mbiemer;
        $this->email = $email;
        $this->fjalekalim = $fjalekalim;
        $this->mosha = $mosha;
        $this->gjinia = $gjinia;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "SELECT * FROM perdorues WHERE Email = :email AND Fjalekalim = :password";
            $query = self::$instance->getConnection()->prepare($sql) ;
            $query -> bindParam(':email', $this->email, PDO::PARAM_STR);
            $query -> bindParam(':password', $this->fjalekalim, PDO::PARAM_STR);

            $query->execute();

            $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

            $this->id = $rezultati[sizeof($rezultati)-1]["id"];
            return $this->id;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @param string $emer
     */
    public function setEmer(string $emer)
    {
        $this->emer = $emer;
    }

    /**
     * @return string
     */
    public function getEmer(): string
    {
        return $this->emer;
    }

    /**
     * @param string $mbiemer
     */
    public function setMbiemer(string $mbiemer)
    {
        try{
            $this->mbiemer = $mbiemer;

            self::$instance = DBConnect::getInstance();

            $sql = "UPDATE perdorues SET Mbiemer = :mbiemer
                WHERE id = :id";
            $query = self::$instance->getConnection()->prepare($sql);
            $query->bindParam(':id',$this->id, PDO::PARAM_INT);
            $query->bindParam(':mbiemer',$this->mbiemer, PDO::PARAM_STR);

            $this->mbiemer = $mbiemer;
            $query->execute();
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @return string
     */
    public function getMbiemer(): string
    {
        return $this->mbiemer;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        try{
            $this->email = $email;

            self::$instance = DBConnect::getInstance();

            $sql = "UPDATE perdorues SET Email = :email
                WHERE id = :id";
            $query = self::$instance->getConnection()->prepare($sql);
            $query->bindParam(':id',$this->id, PDO::PARAM_INT);
            $query->bindParam(':email',$this->email, PDO::PARAM_STR);

            $this->email = $email;
            $query->execute();
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->mbiemer;
    }

    /**
     * @param string $fjalekalim
     */
    public function setFjalekalim(string $fjalekalim)
    {
        try{
            $this->fjalekalim = $fjalekalim;

            self::$instance = DBConnect::getInstance();

            $sql = "UPDATE perdorues SET Fjalekalim = :fjalekalim
                WHERE id = :id";
            $query = self::$instance->getConnection()->prepare($sql);
            $query->bindParam(':id',$this->id, PDO::PARAM_INT);
            $query->bindParam(':fjalekalim',$this->fjalekalim, PDO::PARAM_STR);

            $this->fjalekalim = $fjalekalim;
            $query->execute();
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @return string
     */
    public function getFjalekalim(): string
    {
        return $this->fjalekalim;
    }

    /**
     * @return string
     */
    public function getTipi(): string
    {
        return $this->tipi;
    }

    /**
     * @param int $mosha
     */
    public function setMosha(int $mosha)
    {
        try{
            $this->mosha = $mosha;

            self::$instance = DBConnect::getInstance();

            $sql = "UPDATE perdorues SET Mosha = :mosha
                WHERE id = :id";
            $query = self::$instance->getConnection()->prepare($sql);
            $query->bindParam(':id',$this->id, PDO::PARAM_INT);
            $query->bindParam(':mosha',$this->mosha, PDO::PARAM_INT);

            $this->mosha = $mosha;
            $query->execute();
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @return int
     */
    public function getMosha(): int
    {
        return $this->mosha;
    }

    /**
     * @param string $gjinia
     */
    public function setGjinia(string $gjinia)
    {
        $this->gjinia =$gjinia;
    }

    /**
     * @return string
     */
    public function getGjinia(): string
    {
        return $this->gjinia;
    }

    public function shtoPerdorues()
    {
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "INSERT INTO perdorues (Emri, Mbiemri, Email, Fjalekalim, Tipi, Mosha, Gjinia)
                VALUES(:emri, :mbiemri, :email, :fjalekalimi, :tipi, :mosha, :gjinia)";
            $query = self::$instance->getConnection()->prepare($sql);
            $query->bindParam(':emri',$this->emer, PDO::PARAM_STR);
            $query->bindParam(':mbiemri',$this->mbiemer, PDO::PARAM_STR);
            $query->bindParam(':email',$this->email, PDO::PARAM_STR);
            $query->bindParam(':fjalekalimi',$this->fjalekalim, PDO::PARAM_STR);
            $query->bindParam(':tipi',$this->tipi, PDO::PARAM_STR);
            $query->bindParam(':mosha',$this->mosha, PDO::PARAM_STR);
            $query->bindParam(':gjinia',$this->gjinia, PDO::PARAM_STR);

            $query->execute();
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public static function shikoPerdorues(){
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "SELECT Emri, Mbiemri, Mosha, Gjinia FROM perdorues WHERE Tipi = :tipi";
            $query = self::$instance->getConnection()->prepare($sql) ;
            $query -> bindParam(':tipi', $tipi);

            $tipi = "Klient";

            $query->execute();

            $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

            return $rezultati;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }
}
