<?php
include_once "DBConnect.php";
include_once "Admin.php";
include_once "Klient.php";

session_start();

if (isset($_POST['LogIn'])){
    $instance = DBConnect::getInstance();
    $email = $_POST['email'];
    $password = $_POST['password'];

    $sql = "SELECT* FROM perdorues  WHERE (Email = :email AND Fjalekalim = :password)";
    $query = $instance->getConnection()->prepare($sql) ;
    $query->bindParam(":email", $email);
    $query->bindParam(":password", $password);
    $query->execute();

    $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

    if ($query->rowCount()>0){
        foreach ($rezultati as $item){
            $emri = $item["Emri"];
            $mbiemri = $item["Mbiemri"];
            $email = $item["Email"];
            $password = $item["Fjalekalim"];
            $tipi = $item["Tipi"];
            $mosha = $item["Mosha"];
            $gjinia = $item["Gjinia"];
        }
        if ($tipi == "Admin"){
            $admin = new Admin($emri,$mbiemri, $email, $password, $mosha, $gjinia);

            $_SESSION["perdoruesId"] = $admin->getId();
            header("Location: NderfaqjaAdmin.php");
        }
        elseif ($tipi == "Klient"){
            $klient= new Klient($emri,$mbiemri, $email, $password, $mosha, $gjinia);

            $_SESSION["perdoruesId"] = $klient->getId();
            header("Location: NderfaqjaKlient.php");
        }
        else{
            echo "<b>Wrong username or password!</b>";
        }
    }
    else{
        echo "No person is registered!";
    }
}
