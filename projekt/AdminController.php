<?php
include_once "Admin.php";
include_once "Kategori.php";
include_once "Produkt.php";

if (isset($_POST["shtoK"])){
    $emerKategorie = $_POST["shtimK"];
    $kategori = new Kategori($emerKategorie);
    $status = $kategori->shtoKategori($kategori);
    if ($status){
        echo "Kategoria u shtua me sukses!";
    }
    else
        echo "Ndodhi nje gabim ne shtimin e kategorise!";
}
if (isset($_POST["ndryshoK"])){
    $emriV = $_POST["kategoriV"];
    $emriR = $_POST["kategoriR"];
    $status = Kategori::modifikoKategori($emriV, $emriR);
    if ($status){
        echo "Emri i kategorise u ndryshua me sukses!";
    }
    else
        echo "Kjo kategori nuk ekziston!";
}
if (isset($_POST["hiqKt"])){
    $kategori = new Kategori($_POST["hiqK"]);
    $status = $kategori->hiqKategori($kategori);
    if ($status){
        echo "Kategoria u fshi me sukses!";
    }
    else
        echo "Kjo kategori nuk ekziston!";
}
if (isset($_POST["shtoP"])){
    $emri = $_POST["emri"];
    $cmimi = (int)$_POST["cmimi"];
    $kategoria = Kategori::kerkoKategori($_POST["kategori"]);
    if ($kategoria == false){
        echo "Kjo kategori nuk ekziston!";
    }
    else{
        $emriK = $kategoria[0]["Emer"];
        $kategori = new Kategori($emriK);
        $produkt = new Produkt($emri, $cmimi, $kategori);
        $status = $produkt->shtoProdukt($produkt);
        if ($status){
            echo "Produkti u shtua me sukses!";
        }
        else
            echo "Ndodhi nje gabim ne shtimin e produktit!";
    }
}
if (isset($_POST["ndryshoP"])){
    $emri = $_POST["emriP"];
    $vlera = $_POST["vlera"];

    $status = Produkt::modifikoCmimProdukti($emri, $vlera);
    if ($status){
        echo "Cmimi u ndryshua me sukses!";
    }
    else
        echo "Ky produkt nuk ekziston!";
}
if (isset($_POST["hiqPr"])){
    $emri = $_POST["hiqP"];
    $status = Produkt::fshiProdukt($emri);
    if ($status){
        echo "Produkti u fshi me sukses!";
    }
    else
        echo "Ky produkt nuk ekziston!";
}
if (isset($_POST["ndryshoKP"])){
    $emerProdukti = $_POST["emriPr"];
    $kategoriRe = new Kategori($_POST["kategoriRe"]);
    $status = Produkt::ndryshoKategoriProduktit($emerProdukti, $kategoriRe);
    if ($status){
        echo "Kategoria u ndryshua me sukses!";
    }
    else
        echo "Produkti ose kategoria nuk ekziston!";
}
if (isset($_POST["shiko"])){
    $kliente = Admin::shikoPerdorues();
    if (empty($kliente)){
        echo "Nuk ka asnje klient te rregjistruar!";
    }
    else{
        ?>
        <html>
        <body>
        <table border="1">
            <tr>
                <th>
                    Emri
                </th>
                <th>
                    Mbiemri
                </th>
                <th>
                    Mosha
                </th>
                <th>
                    Gjinia
                </th>
            </tr>
            <?php
            foreach ($kliente as $klient):
                ?>
                <tr>
                    <td><?php echo $klient["Emri"] ?></td>
                    <td><?php echo $klient["Mbiemri"] ?></td>
                    <td><?php echo $klient["Mosha"] ?></td>
                    <td><?php echo $klient["Gjinia"] ?></td>
                </tr>
            <?php
            endforeach;
            ?>
        </table>
        </body>
        </html>
        <?php
    }
}
if (isset($_POST["LogOut"])){
    session_unset();
    header("Location: LogIn.php");
}