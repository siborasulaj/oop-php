<?php

//namespace Produkt;
//use Produkt\Produkt;

include_once "DBConnect.php";

class Kategori{
    private static $instance;
    private $id;
    private $emer;

    /**
     * Kategori constructor.
     * @param $emer
     */
    public function __construct($emer){
        $this->emer = $emer;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        try {
            self::$instance= DBConnect::getInstance();

            $sql = "SELECT * FROM kategori WHERE Emer = :emer";
            $query = self::$instance->getConnection()->prepare($sql) ;
            $query -> bindParam(':emer', $this->emer, PDO::PARAM_STR);

            $query->execute();

            $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

            if (empty($rezultati)){
                return false;
            }
            else{
                $k_id = $rezultati[0]["id"];
                $this->id = $k_id;
                return $this->id;
            }
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @param mixed $emer
     */
    public function setEmer($emer)
    {
        $this->emer = $emer;
    }

    /**
     * @return mixed
     */
    public function getEmer()
    {
        return $this->emer;
    }
    /**
     * @param Kategori $kategori
     */
    public function shtoKategori(Kategori $kategori){
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "INSERT INTO kategori (Emer) VALUES (:emri)";
            $query = self::$instance->getConnection()->prepare($sql);
            $query->bindParam(':emri',$emer_kategorie, PDO::PARAM_STR);

            $emer_kategorie = $kategori->getEmer();

            $query->execute();

            $lastInsertId = self::$instance->getConnection()->lastInsertId();
            if($lastInsertId>0)
            {
                return true;
            }
            else
                return false;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }

    }

    /**
     * @param Kategori $kategori
     */
    public function hiqKategori(Kategori $kategori){
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "DELETE FROM kategori WHERE id = :id";
            $query = self::$instance->getConnection()->prepare($sql);
            $query->bindParam(':id', $id_kategorie, PDO::PARAM_INT);

            $id_kategorie = $kategori->getId();

            $query->execute();

            if($query -> rowCount() > 0)
            {
                $count = $query -> rowCount() ;
                return true;
            }
            else
                return false;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @param Kategori $kategori
     * @param string $emer_kategorie
     */
    public static function modifikoKategori(string $emriV, string $emer_kategorie){
        try {
            $kategorite = Kategori::kerkoKategori($emriV);
            self::$instance = DBConnect::getInstance();

            foreach ($kategorite as $kategori){
                $sql = "UPDATE kategori SET Emer = :emer
                WHERE id = :idV";
                $query = self::$instance->getConnection()->prepare($sql);
                $query->bindParam(':emer',$emer_kategorie, PDO::PARAM_STR);
                $query->bindParam(':idV',$idV, PDO::PARAM_INT);

                $idV = $kategori["id"];

                $query->execute();

                if($query -> rowCount() > 0)
                {
                    $count = $query -> rowCount() ;
                    return true;
                }
                else
                    return false;
            }
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public static function shikoKategorite(){
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "SELECT * FROM kategori";
            $query = self::$instance->getConnection()->prepare($sql) ;

            $query->execute();

            $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

            return $rezultati;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }
    public static function kerkoKategori(string $emer_kategorie){
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "SELECT * FROM kategori WHERE Emer = :emri";
            $query = self::$instance->getConnection()->prepare($sql) ;
            $query -> bindParam(':emri', $emer_kategorie);

            $query->execute();

            $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

            return $rezultati;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

}