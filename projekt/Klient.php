<?php

include_once "Perdorues.php";
include_once "Kategori.php";

//use Perdorues\Perdorues;
//use Produkt\Kategori;
//use Produkt\Produkt;

class Klient extends Perdorues{
    private static $instance;
    private $id;
    private $emer;
    private $mbiemer;
    private $email;
    private $fjalekalim;
    private $tipi = "Klient";
    private $mosha;
    private $gjinia;

    /**
     * Klient constructor.
     * @param string $emer
     * @param string $mbiemer
     * @param string $email
     * @param string $fjalekalim
     * @param int $mosha
     * @param string $gjinia
     */
    public function __construct(string $emer, string $mbiemer, string $email, string $fjalekalim, int $mosha, string $gjinia){
        $this->emer = $emer;
        $this->mbiemer = $mbiemer;
        $this->email = $email;
        $this->fjalekalim = $fjalekalim;
        $this->mosha = $mosha;
        $this->gjinia = $gjinia;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "SELECT * FROM perdorues WHERE Email = :email AND Fjalekalim = :password";
            $query = self::$instance->getConnection()->prepare($sql) ;
            $query -> bindParam(':email', $this->email, PDO::PARAM_STR);
            $query -> bindParam(':password', $this->fjalekalim, PDO::PARAM_STR);

            $query->execute();

            $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

            $this->id = $rezultati[sizeof($rezultati)-1]["id"];
            return $this->id;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    /**
     * @param string $emer
     */
    public function setEmer(string $emer)
    {
            $this->emer = $emer;
    }

    /**
     * @return string
     */
    public function getEmer(): string
    {
        return $this->emer;
    }

    /**
     * @param string $mbiemer
     */
    public function setMbiemer(string $mbiemer)
    {
            $this->mbiemer = $mbiemer;
    }

    /**
     * @return string
     */
    public function getMbiemer(): string
    {
        return $this->mbiemer;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
            $this->email = $email;

    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->mbiemer;
    }

    /**
     * @param string $fjalekalim
     */
    public function setFjalekalim(string $fjalekalim)
    {
            $this->fjalekalim = $fjalekalim;
    }

    /**
     * @return string
     */
    public function getFjalekalim(): string
    {
        return $this->fjalekalim;
    }

    /**
     * @return string
     */
    public function getTipi(): string
    {
        return $this->tipi;
    }

    /**
     * @param int $mosha
     */
    public function setMosha(int $mosha)
    {
            $this->mosha = $mosha;
    }

    /**
     * @return int
     */
    public function getMosha(): int
    {
        return $this->mosha;
    }

    /**
     * @param string $gjinia
     */
    public function setGjinia(string $gjinia)
    {
        $this->gjinia = $gjinia;
    }

    /**
     * @return string
     */
    public function getGjinia(): string
    {
        return $this->gjinia;
    }
    public function shtoPerdorues()
    {
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "INSERT INTO perdorues (Emri, Mbiemri, Email, Fjalekalim, Tipi, Mosha, Gjinia)
                VALUES(:emri, :mbiemri, :email, :fjalekalimi, :tipi, :mosha, :gjinia)";
            $query = self::$instance->getConnection()->prepare($sql);
            $query->bindParam(':emri',$this->emer, PDO::PARAM_STR);
            $query->bindParam(':mbiemri',$this->mbiemer, PDO::PARAM_STR);
            $query->bindParam(':email',$this->email, PDO::PARAM_STR);
            $query->bindParam(':fjalekalimi',$this->fjalekalim, PDO::PARAM_STR);
            $query->bindParam(':tipi',$this->tipi, PDO::PARAM_STR);
            $query->bindParam(':mosha',$this->mosha, PDO::PARAM_STR);
            $query->bindParam(':gjinia',$this->gjinia, PDO::PARAM_STR);

            $query->execute();
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }
    public static function modifikoTeDhena($idP, $emri, $mbiemri, $email, $password, $mosha){
        try {
            self::$instance = DBConnect::getInstance();

            $sql = "UPDATE perdorues SET Emri=:emri, Mbiemri=:mbiemri, Email=:email, Fjalekalim=:fjalekalimi, Mosha=:mosha WHERE id=:id";
            $query = self::$instance->getConnection()->prepare($sql);
            $query->bindParam(':emri',$emri, PDO::PARAM_STR);
            $query->bindParam(':mbiemri',$mbiemri, PDO::PARAM_STR);
            $query->bindParam(':email',$email, PDO::PARAM_STR);
            $query->bindParam(':fjalekalimi',$password, PDO::PARAM_STR);
            $query->bindParam(':mosha',$mosha, PDO::PARAM_STR);
            $query->bindParam(':id',$idP, PDO::PARAM_INT);

            $query->execute();

            if($query -> rowCount() > 0)
            {
                $count = $query -> rowCount() ;
                return true;
            }
            else
                return false;
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }
}