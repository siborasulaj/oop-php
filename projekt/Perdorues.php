<?php

include_once "DBConnect.php";

//namespace Perdorues;

abstract class Perdorues
{
    private $instance;
    private $id;
    private $emer;
    private $mbiemer;
    private $email;
    private $fjalekalim;
    private $tipi;
    private $mosha;
    private $gjinia;

    abstract function setId(string $id);
    abstract function getId(): string;
    abstract function setEmer(string $emer);
    abstract function getEmer(): string;
    abstract function setMbiemer(string $mbiemer);
    abstract function getMbiemer(): string;
    abstract function setEmail(string $email);
    abstract function getEmail(): string;
    abstract function setFjalekalim(string $fjalekalim);
    abstract function getFjalekalim(): string;
    abstract function getTipi(): string;
    abstract function setMosha(int $mosha);
    abstract function getMosha(): int;
    abstract function setGjinia(string $gjinia);
    abstract function getGjinia(): string;

    public function shtoPerdorues(){
        try {
            $this->instance = DBConnect::getInstance();

            $sql = "INSERT INTO perdorues (Emri, Mbiemri, Email, Fjalekalim, Tipi, Mosha, Gjinia)
                VALUES(:emri, :mbiemri, :email, :fjalekalimi, :tipi, :mosha, :gjinia)";
            $query = $this->instance->getConnection()->prepare($sql);
            $query->bindParam(':emri',$this->emer, PDO::PARAM_STR);
            $query->bindParam(':mbiemri',$this->mbiemer, PDO::PARAM_STR);
            $query->bindParam(':email',$this->email, PDO::PARAM_STR);
            $query->bindParam(':fjalekalimi',$this->fjalekalim, PDO::PARAM_STR);
            $query->bindParam(':tipi',$this->tipi, PDO::PARAM_STR);
            $query->bindParam(':mosha',$this->mosha, PDO::PARAM_STR);
            $query->bindParam(':gjinia',$this->gjinia, PDO::PARAM_STR);

            $query->execute();
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }
}