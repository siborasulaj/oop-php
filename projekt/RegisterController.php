<?php
include_once "Admin.php";
include_once "Klient.php";

session_start();

if (isset($_POST["Register"])){
    if (strlen($_POST["password"])<8){
        echo "Fjalekalimi duhet te jete minimalisht 8 karaktere!";
    }
    else{
        $emri = $_POST["emri"];
        $mbiemri = $_POST["mbiemri"];
        $email = $_POST["email"];
        $password = $_POST["password"];
        $tipi = $_POST["tipi"];
        $mosha = $_POST["mosha"];
        $gjinia = $_POST["gjinia"];

        if (empty($emri) || empty($mbiemri) || empty($email) || empty($mosha) || empty($gjinia)){
            echo "Ju lutem plotesoni te gjitha fushat!";
        }
        else{
            if ($tipi == "Admin"){
                $perdorues = new Admin($emri, $mbiemri, $email, $password, $mosha, $gjinia);
            }
            else{
                $perdorues = new Klient($emri, $mbiemri, $email, $password, $mosha, $gjinia);
            }
            $perdorues->shtoPerdorues();
            $_SESSION["perdoruesId"] = $perdorues->getId();

            if ($perdorues instanceof Admin){
                header("Location: NderfaqjaAdmin.php");
            }
            elseif ($perdorues instanceof Klient){
                header("Location: NderfaqjaKlient.php");
            }
        }
    }
}