<?php

define("DB_HOST", "localhost");
define("DB_NAME", "projekt_shero");
define("DB_USER", "Sibora");
define("DB_PASS", "test1");

class DBConnect{
    private static $conn;
    private static $instance = null;

    private function __construct(){
        try {
            self::$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,
                DB_USER, DB_PASS,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
        }
        catch (PDOException $e){
            exit("Error: " . $e->getMessage());
        }
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new DBConnect();
        }
        return self::$instance;
    }
    public function getConnection(){
        return self::$conn;
    }
}
