<?php
include "Liber.php";

$shoqeruesja = new Liber();
$shoqeruesja->titull = "Shoqeruesja";
$shoqeruesja->autor = "John Galsworthy";
$shoqeruesja->titulliOrigjinal = "Maid in Waiting";
$shoqeruesja->gjuhaOrigjinale = "Anglisht";
$shoqeruesja->shtepiaBotuese = "Pegi";

$krenariDheParagjykim = new Liber();
$krenariDheParagjykim->titull = "Krenari dhe paragjykim";
$krenariDheParagjykim->autor = "Jane Austin";
$krenariDheParagjykim->titulliOrigjinal = "Pride and Prejudice";
$krenariDheParagjykim->gjuhaOrigjinale = "Anglisht";
$krenariDheParagjykim->shtepiaBotuese = "Toena";

$prilliIThyer = new Liber();
$prilliIThyer->titull = "Prilli i Thyer";
$prilliIThyer->autor = "Ismail Kadare";
$prilliIThyer->titulliOrigjinal = "Prilli i Thyer";
$prilliIThyer->gjuhaOrigjinale = "Shqip";
$prilliIThyer->shtepiaBotuese = "Onufri";

echo "Titulli: ".$shoqeruesja->titull."<br>";
echo "Autori: ".$shoqeruesja->autor."<br>";
echo "Titulli Origjinal: ".$shoqeruesja->titulliOrigjinal."<br>";
echo "Gjuha Origjinale: ".$shoqeruesja->gjuhaOrigjinale."<br>";
echo "Shtepia Botuese: ".$shoqeruesja->shtepiaBotuese."<br><br>";

echo "Titulli: ".$krenariDheParagjykim->titull."<br>";
echo "Autori: ".$krenariDheParagjykim->autor."<br>";
echo "Titulli Origjinal: ".$krenariDheParagjykim->titulliOrigjinal."<br>";
echo "Gjuha Origjinale: ".$krenariDheParagjykim->gjuhaOrigjinale."<br>";
echo "Shtepia Botuese: ".$krenariDheParagjykim->shtepiaBotuese."<br><br>";

echo "Titulli: ".$prilliIThyer->titull."<br>";
echo "Autori: ".$prilliIThyer->autor."<br>";
echo "Titulli Origjinal: ".$prilliIThyer->titulliOrigjinal."<br>";
echo "Gjuha Origjinale: ".$prilliIThyer->gjuhaOrigjinale."<br>";
echo "Shtepia Botuese: ".$prilliIThyer->shtepiaBotuese."<br><br>";