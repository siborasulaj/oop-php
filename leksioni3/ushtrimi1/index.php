<?php
include("Laptop.php");
$laptop = new Laptop;
$m = $laptop->marka;
$mod = $laptop->modeli = "Latitude E5550";
$p = $laptop->procesori = "Intel i5";
$s = $laptop->sistemiIOperimit;
$r = $laptop->RAM = "12 GB";

echo "Laptopi me marke ".$m.", model ".$mod." ka procesor te tipit ".$p." dhe RAM ".$r.". Sistemi i operimit eshte ".$s.".<br>";

$laptop->marka = "HP";
$laptop->modeli = "EliteBook 840";
$laptop->RAM = "6 GB";
$laptop->procesori = "Intel i6";
$laptop->sistemiIOperimit = "Windows 7";

echo "Laptopi me marke ".$laptop->marka.", model ".$laptop->modeli." ka procesor te tipit ".$laptop->procesori." dhe RAM ".$laptop->RAM.". Sistemi i operimit eshte ".$laptop->sistemiIOperimit.".";