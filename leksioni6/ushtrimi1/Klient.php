<?php
include_once "Perdorues.php";

class Klient implements Perdorues {
    private $emri;

    public function __construct($emri){
        $this->emri = $emri;
    }

    /**
     * @param mixed $emri
     */
    public function setEmri($emri)
    {
        $this->emri = $emri;
    }

    /**
     * @return mixed
     */
    public function getEmri()
    {
        return $this->emri;
    }
}