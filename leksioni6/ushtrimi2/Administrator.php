<?php
include "AutorInterface.php";
include "EditorInterface.php";

class Administrator implements AutorInterface, EditorInterface {
    private $emer;
    private $lloji;
    private $permbajtje;
    private $titull;
    private $liber;

    public function __construct($emer, $lloji){
        $this->emer = $emer;
        $this->lloji = $lloji;
    }

    /**
     * @param mixed $emer
     */
    public function setEmer($emer)
    {
        $this->emer = $emer;
    }

    /**
     * @return mixed
     */
    public function getEmer()
    {
        return $this->emer;
    }

    /**
     * @param mixed $lloji
     */
    public function setLloji($lloji)
    {
        $this->lloji = $lloji;
    }

    /**
     * @return mixed
     */
    public function getLloji()
    {
        return $this->lloji;
    }
    public function ndryshoPermbajtje($permbajtje)
    {
        if ($this->lloji == "AE" || $this->lloji == "E"){
            return $this->permbajtje = $permbajtje;
        }
        else{
            return "Nuk keni akses!";
        }
    }
    public function addLiber($liber)
    {
        if ($this->lloji == "AE" || $this->lloji == "E"){
            return $this->liber = $liber;
        }
        else{
            return "Nuk keni akses!";
        }
    }
    public function setAutor($autor)
    {
        if ($this->lloji == "AE" || $this->lloji == "A"){
            return $this->emer = $autor;
        }
        else{
            return "Nuk keni akses!";
        }
    }
    public function setTitull($titull)
    {
        if ($this->lloji == "AE" || $this->lloji == "A"){
            return $this->titull = $titull;
        }
        else{
            return "Nuk keni akses!";
        }
    }
    public function checkLloji(){
        $privilegjet = "";
        switch ($this->lloji){
            case "AE":
                $privilegjet = "Perdoruesit ".$this->emer." i lejohen privilegjet e tipit Autor dhe Editues: 
                Shtimi i librit, Vendosja e Autoresise, Vendosja e Titullit, Ndryshimi i permbajtjes.";
                break;
            case "A":
                $privilegjet = "Perdoruesit ".$this->emer." i lejohen privilegjet e tipit Autor: 
                Shtimi i librit, Vendosja e Autoresise, Vendosja e Titullit.";
                break;
            case "E":
                $privilegjet = "Perdoruesit ".$this->emer." i lejohen privilegjet e tipit Editues: Ndryshimi i permbajtjes.";
                break;
            default:
                $privilegjet = "Ju lutem vendosni llojin e perdoruesit!";
        }
        return $privilegjet;
    }
}