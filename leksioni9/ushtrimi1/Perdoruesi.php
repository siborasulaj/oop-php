<?php
class Perdoruesi{
    private $emri;
    private $mosha;

    public function __construct( $emri, $mosha){
        $this->emri = $emri;
        $this->mosha = $mosha;
        if(strlen($this->emri)<3){
            throw new Exception("Emri nuk mund te kete me pak se 3 shkronja!");
        }
        if ($this->mosha<=0 || !is_numeric($this->mosha)){
            throw new Exception("Mosha duhet te jete nje numer me i madh se 0!");
        }
    }

    /**
     * @param string $emri
     */
    public function setEmri(string $emri)
    {
        $this->emri = $emri;
        if(strlen($this->emri)<3){
            throw new Exception("Emri nuk mund te kete me pak se 3 shkronja!");
        }
    }

    /**
     * @return string
     */
    public function getEmri(): string
    {
        return $this->emri;
    }

    /**
     * @param int $mosha
     */
    public function setMosha(int $mosha)
    {
        $this->mosha = $mosha;
        if ($this->mosha<=0 || !is_numeric($this->mosha)){
            throw new Exception("Mosha duhet te jete nje numer me i madh se 0!");
        }
    }

    /**
     * @return int
     */
    public function getMosha(): int
    {
        return $this->mosha;
    }
}