<?php

include_once "Perdoruesi.php";

$perdoruesit = array(
    array("Ben", 4),
    array("Eva", 28),
    array("li", 29),
    array("Catie", "ska lindur akoma"),
    array("Sue", 1.5)
);
foreach ($perdoruesit as $perdorues=>$value) {
    try {
        $perdoruesi = new Perdoruesi($value[0], $value[1]);
        echo $perdoruesi->getEmri() . " " . $perdoruesi->getMosha() . "<br>";
    } catch (Exception $e) {
        echo $e->getMessage() . " <br>File: " . $e->getFile() . " <br>Line: " . $e->getLine() . "<br>";
    }
}