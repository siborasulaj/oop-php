<?php

include_once "Hero.php";

class Magjistar extends Hero{
    protected $tipi = "Magjistar";
    protected $arma = "Magji";
    protected $emri;
    protected $niveli;
    protected $fuqia = 55;
    protected $mbrojtja = 40;
    protected $inteligjenca = 60;
    protected $rezistenca = 30;

    /**
     * Magjistar constructor.
     * @param $emri
     * @param $niveli
     * @throws Exception
     */
    public function __construct($emri, $niveli){
        $this->emri = $emri;
        $this->niveli = $niveli;
        if($this->niveli <= 0){
            throw new Exception("Niveli nuk mund te jete me pak se 1!");
        }
        else{
            $this->llogaritAtributet($this->niveli);
        }
    }

    /**
     * @param string $emri
     * @return mixed|void
     */
    public function setEmri(string $emri)
    {
        $this->emri = $emri;
    }

    /**
     * @return string
     */
    public function getEmri(): string
    {
        return $this->emri;
    }

    /**
     * @param int $niveli
     * @return mixed|void
     */
    public function setNiveli(int $niveli)
    {
        $this->niveli = $niveli;
        $this->llogaritAtributet($niveli);
    }

    /**
     * @return int
     */
    public function getNiveli(): int
    {
        return $this->niveli;
    }

    /**
     * @return string
     */
    public function getTipi(): string
    {
        return $this->tipi;
    }

    /**
     * @return string
     */
    public function getArma(): string
    {
        return $this->arma;
    }

    /**
     * @return int
     */
    public function getFuqia(): int
    {
        return $this->fuqia;
    }

    /**
     * @return int
     */
    public function getMbrojtja(): int
    {
        return $this->mbrojtja;
    }

    /**
     * @return int
     */
    public function getInteligjenca(): int
    {
        return $this->inteligjenca;
    }

    /**
     * @return int
     */
    public function getRezistenca(): int
    {
        return $this->rezistenca;
    }

    /**
     * @param int $niveli
     */
    public function llogaritAtributet(int $niveli)
    {
        parent::llogaritAtributet($niveli);
    }
}