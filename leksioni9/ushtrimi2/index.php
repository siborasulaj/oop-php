<?php

include_once "Luftetar.php";
include_once "Magjistar.php";
include_once "Shigjetar.php";
include_once "Grupet.php";

/**
 * @param array $heronj
 * @return array
 * @throws Exception
 */
function krijoGrup(array $heronj){
    $grupi = array();
    foreach ($heronj as $key){
        if ($key["tipi"] == "Shigjetar"){
            $hero = new Shigjetar($key["emri"], $key["niveli"]);
        }
        elseif ($key["tipi"] == "Magjistar"){
            $hero = new Magjistar($key["emri"], $key["niveli"]);
        }
        elseif ($key["tipi"] == "Luftetar"){
            $hero = new Luftetar($key["emri"], $key["niveli"]);
        }
        else{
            throw new Exception("Vendosni nje nga tipet e paracaktuara te heroit!");
        }
        array_push($grupi, $hero);
    }
    return $grupi;
}

/**
 * @param Grupet $grupi
 */
function afishoAnetaret(Grupet $grupi){
    $heronj = $grupi->getAnetaret();
    ?>
    <html>
    <body>
    <table border="1">
        <tr>
            <th>Emri</th>
            <th>Tipi</th>
            <th>Niveli</th>
            <th>Arma</th>
            <th>Fuqia</th>
            <th>Inteligjenca</th>
            <th>Mbrojtja</th>
            <th>Rezistenca</th>
        </tr>
        <?php
        foreach ($heronj as $value):
            ?>
            <tr>
                <td><?php echo $value->getEmri(); ?></td>
                <td><?php echo $value->getTipi(); ?></td>
                <td><?php echo $value->getNiveli(); ?></td>
                <td><?php echo $value->getArma(); ?></td>
                <td><?php echo $value->getFuqia(); ?></td>
                <td><?php echo $value->getInteligjenca(); ?></td>
                <td><?php echo $value->getMbrojtja(); ?></td>
                <td><?php echo $value->getrezistenca(); ?></td>
            </tr>
        <?php endforeach;?>
    </table>
    </body>
    </html>
    <?php
}

$heronj1 = array(
    array("tipi" => "Shigjetar",
        "emri" => "Zeus",
      "niveli" => 5),
    array("tipi" => "Luftetar",
        "emri" => "Hera",
        "niveli" => 5),
    array("tipi" => "Magjistar",
        "emri" => "Aferdita",
        "niveli" => 3),
    array("tipi" => "Luftetar",
        "emri" => "Ares",
        "niveli" => 4),
);

$heronj2 = array(
    array("tipi" => "Magjistar",
        "emri" => "Athina",
        "niveli" => 4),
    array("tipi" => "Shigjetar",
        "emri" => "Hadi",
        "niveli" => 6),
    array("tipi" => "Shigjetar",
        "emri" => "Poseidon",
        "niveli" => 5),
    array("tipi" => "Magjistar",
        "emri" => "Demetra",
        "niveli" => 2)
);

try{
    $grupi1 = krijoGrup($heronj1);
    $grupi2 = krijoGrup($heronj2);

    $grup1 = new Grupet("Grupi 1", $grupi1);
    $grup2 = new Grupet("Grupi 2", $grupi2);

    echo "<b>".$grup1->getEmri()."</b><br><br>";
    afishoAnetaret($grup1);
    echo "<br><b>".$grup2->getEmri()."</b><br><br>";
    afishoAnetaret($grup2);
}
catch (Exception $e){
    echo $e->getMessage();
}