<?php

include_once "Hero.php";

class Grupet{
    private $emri;
    private $anetaret;

    public function __construct(string $emri, array $anetaret){
        $this->emri = $emri;
        foreach ($anetaret as $value){
            if (!($value instanceof Hero)){
                throw new Exception("Vlerat duhet te jene te tipit Hero!");
            }
            else{
                $this->anetaret = $anetaret;
            }
        }
    }

    /**
     * @param string $emri
     */
    public function setEmri(string $emri)
    {
        $this->emri = $emri;
    }

    /**
     * @return string
     */
    public function getEmri(): string
    {
        return $this->emri;
    }

    /**
     * @param array $anetaret
     */
    public function setAnetaret(array $anetaret)
    {
        foreach ($anetaret as $value){
            if (!($value instanceof Hero)){
                throw new Exception("Vlerat duhet te jene te tipit Hero!");
            }
            else{
                $this->anetaret = $anetaret;
            }
        }
    }

    /**
     * @return array
     */
    public function getAnetaret(): array
    {
        return $this->anetaret;
    }
    public function afishoAnetaret(){
        ?>
        <html>
        <body>
        <table border="1">
        <tr>
            <th>Emri</th>
            <th>Tipi</th>
            <th>Niveli</th>
            <th>Arma</th>
            <th>Fuqia</th>
            <th>Inteligjenca</th>
            <th>Mbrojtja</th>
            <th>Rezistenca</th>
        </tr>
        <?php
        foreach ($this->anetaret as $value):
        ?>
        <tr>
            <td><?php echo $value->getEmri(); ?></td>
            <td><?php echo $value->getTipi(); ?></td>
            <td><?php echo $value->getNiveli(); ?></td>
            <td><?php echo $value->getArma(); ?></td>
            <td><?php echo $value->getFuqia(); ?></td>
            <td><?php echo $value->getInteligjenca(); ?></td>
            <td><?php echo $value->getMbrojtja(); ?></td>
            <td><?php echo $value->getrezistenca(); ?></td>
        </tr>
        <?php endforeach;?>
    </table>
    </body>
    </html>
    <?php
    }
}