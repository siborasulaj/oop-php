<?php

interface HeroiInterface{
    function setEmri(string $emri);
    function getEmri(): string;
    function getTipi(): string;
    function setNiveli(int $niveli);
    function getNiveli(): int;
    function llogaritAtributet(int $niveli);
}