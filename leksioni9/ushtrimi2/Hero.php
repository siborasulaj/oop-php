<?php

abstract class Hero{
    protected $tipi;
    protected $arma;
    protected $emri;
    protected $niveli;
    protected $fuqia;
    protected $mbrojtja;
    protected $inteligjenca;
    protected $rezistenca;

    /**
     * @param mixed $tipi
     */
    public function setTipi($tipi)
    {
        $this->tipi = $tipi;
    }
    abstract function getTipi(): string;

    /**
     * @param mixed $arma
     */
    public function setArma($arma)
    {
        $this->arma = $arma;
    }

    /**
     * @return mixed
     */
    public function getArma()
    {
        return $this->arma;
    }

    /**
     * @param string $emri
     * @return mixed
     */
    abstract function setEmri(string $emri);

    /**
     * @return string
     */
    abstract function getEmri(): string;

    /**
     * @param int $niveli
     * @return mixed
     */
    abstract function setNiveli(int $niveli);

    /**
     * @return int
     */
    abstract function getNiveli(): int;

    /**
     * @param mixed $fuqia
     */
    public function setFuqia($fuqia)
    {
        $this->fuqia = $fuqia;
    }

    /**
     * @return mixed
     */
    public function getFuqia()
    {
        return $this->fuqia;
    }

    /**
     * @param mixed $mbrojtja
     */
    public function setMbrojtja($mbrojtja)
    {
        $this->mbrojtja = $mbrojtja;
    }

    /**
     * @return mixed
     */
    public function getMbrojtja()
    {
        return $this->mbrojtja;
    }

    /**
     * @param mixed $inteligjenca
     */
    public function setInteligjenca($inteligjenca)
    {
        $this->inteligjenca = $inteligjenca;
    }

    /**
     * @return mixed
     */
    public function getInteligjenca()
    {
        return $this->inteligjenca;
    }

    /**
     * @param mixed $rezistenca
     */
    public function setRezistenca($rezistenca)
    {
        $this->rezistenca = $rezistenca;
    }

    /**
     * @return mixed
     */
    public function getRezistenca()
    {
        return $this->rezistenca;
    }
    public function llogaritAtributet(int $niveli){
        if ($niveli > 1){
            for ($i = 2; $i <= $niveli; $i++){
                if ($i % 2 ==1){
                    $this->fuqia += 5;
                    $this->mbrojtja += 7;
                    $this->inteligjenca += 3;
                    $this->rezistenca += 4;
                }
                else{
                    $this->fuqia += 8;
                    $this->mbrojtja += 10;
                    $this->inteligjenca += 6;
                    $this->rezistenca += 7;
                }
            }
        }
    }
}