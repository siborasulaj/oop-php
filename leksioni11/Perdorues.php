<?php

include_once "DBConnect.php";

class Perdorues{
    private $emri;
    private $mbiemri;
    private $qyteti;
    private $dbConn;
    private $id;

    public function __construct($emri, $mbiemri, $qyteti){
        $this->emri = $emri;
        $this->mbiemri = $mbiemri;
        $this->qyteti = $qyteti;
    }

    /**
     * @param mixed $emri
     */
    public function setEmri($emri)
    {
        $this->emri = $emri;
    }

    /**
     * @return mixed
     */
    public function getEmri()
    {
        return $this->emri;
    }

    /**
     * @param mixed $mbiemri
     */
    public function setMbiemri($mbiemri)
    {
        $this->mbiemri = $mbiemri;
    }

    /**
     * @return mixed
     */
    public function getMbiemri()
    {
        return $this->mbiemri;
    }

    /**
     * @param mixed $qyteti
     */
    public function setQyteti($qyteti)
    {
        $this->qyteti = $qyteti;
    }

    /**
     * @return mixed
     */
    public function getQyteti()
    {
        return $this->qyteti;
    }
    public function shto(){
        $this->dbConn = new DBConnect();
        $sql = "INSERT INTO perdoruesit(emri, mbiemri, qyteti)
                VALUES(:emri,:mbiemri,:qyteti)";
        $query = $this->dbConn->getDb()-> prepare($sql);
        $query->bindParam(':emri',$this->emri, PDO::PARAM_STR);
        $query->bindParam(':mbiemri',$this->mbiemri, PDO::PARAM_STR);
        $query->bindParam(':qyteti',$this->qyteti, PDO::PARAM_STR);

        $query->execute();
    }
    public function afishoTeGjithe(){
        $this->dbConn = new DBConnect();
        $sql = "SELECT * FROM perdoruesit";
        $query = $this->dbConn->getDb()-> prepare($sql);
        $query->execute();
        $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

        return $rezultati;
    }
    public function afishoPerdoruesSipasId($id){
        $this->dbConn = new DBConnect();
        $sql = "SELECT * FROM perdoruesit WHERE id= :id";
        $query = $this->dbConn->getDb()->prepare($sql) ;
        $query -> bindParam(':id', $this->id);
        $this->id = $id;
        $query->execute();
        $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

        return $rezultati;
    }
    public function modifiko($emri, $mbiemri, $qyteti){
        $this->dbConn = new DBConnect();
        $sql = "UPDATE perdoruesit SET qyteti = :qyteti , mbiemri = :mbiemri
                WHERE emri = :emri";
        $query = $this->dbConn->getDb()->prepare($sql);
        $query->bindParam(':emri',$this->emri, PDO::PARAM_STR);
        $query->bindParam(':mbiemri',$this->mbiemri, PDO::PARAM_STR);
        $query->bindParam(':qyteti',$this->qyteti, PDO::PARAM_STR);

        $this->emri = $emri;
        $this->mbiemri = $mbiemri;
        $this->qyteti = $qyteti;
        $query->execute();
    }
    public function fshi($id){
        $this->dbConn = new DBConnect();
        $sql = "DELETE FROM perdoruesit WHERE id = :id";
        $query = $this->dbConn->getDb()->prepare($sql);
        $query->bindParam(':id', $this->id, PDO::PARAM_INT);
        $this->id = $id;
        $query->execute();
    }
}