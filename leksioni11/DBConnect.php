<?php

define("DB_HOST", "localhost");
define("DB_NAME", "shero_php");
define("DB_USER", "Sibora");
define("DB_PASS", "test1");

class DBConnect{
    private $db;

    public function __construct(){
        try
        {
            $this->db = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,
                DB_USER, DB_PASS,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

            $perdorues = "CREATE TABLE IF NOT EXISTS perdoruesit(
                            id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                            Emri varchar(60) DEFAULT NULL,
                            Mbiemri varchar(14) DEFAULT NULL,
                            Qyteti varchar(60) DEFAULT NULL,
                            PRIMARY KEY (id)
                            )";

            $this->db->exec($perdorues);
        }
        catch (PDOException $e){
            exit("Error: " . $e->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function getDb()
    {
        return $this->db;
    }
}