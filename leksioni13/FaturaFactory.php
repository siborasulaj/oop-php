<?php

include_once "Fature.php";

class FaturaFactory{
    public static function create($klient){
        return new Fature($klient);
    }
}