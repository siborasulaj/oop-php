<?php

include_once "DBConnect.php";

class Porosi{
    private $instance;
    private $id;
    private $klient_id;
    private $artikull;
    private $sasia;
    private $cmimi;
    private $shuma = 0;

    public function __construct(Klient $klient, $artikull, $sasia, $cmimi)
    {
        $this->klient_id = $klient->getId();
        $this->artikull = $artikull;
        $this->sasia = $sasia;
        $this->cmimi = $cmimi;
        $this->shuma = $sasia * $cmimi;
    }

    /**
     * @param mixed $artikull
     */
    public function setArtikull($artikull)
    {
        $this->artikull = $artikull;
    }

    /**
     * @return mixed
     */
    public function getArtikull()
    {
        return $this->artikull;
    }

    /**
     * @param mixed $sasia
     */
    public function setSasia($sasia)
    {
        $this->sasia = $sasia;
    }

    /**
     * @return mixed
     */
    public function getSasia()
    {
        return $this->sasia;
    }

    /**
     * @param mixed $cmimi
     */
    public function setCmimi($cmimi)
    {
        $this->cmimi = $cmimi;
    }

    /**
     * @return mixed
     */
    public function getCmimi()
    {
        return $this->cmimi;
    }
    public function setId()
    {
        $this->instance = DBConnect::getInstance();

        $sql = "SELECT * FROM porosi WHERE Artikulli = :artikulli AND Klient_id = :klient_id";
        $query = $this->instance->getConnection()->prepare($sql) ;
        $query -> bindParam(':artikulli', $this->artikull);
        $query -> bindParam(':klient_id', $this->klient_id);

        $query->execute();

        $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

        $this->id = $rezultati[0]["id"];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    public function insert(){
        $this->instance = DBConnect::getInstance();

        $sql = "INSERT INTO porosi (Klient_id, Artikulli, Sasia, Cmimi, Shuma) 
                VALUES (:klient_id,:artikulli, :sasia, :cmimi, :shuma)";
        $query = $this->instance->getConnection()->prepare($sql);
        $query->bindParam(':klient_id',$this->klient_id, PDO::PARAM_INT);
        $query->bindParam(':artikulli', $this->artikull, PDO::PARAM_STR);
        $query->bindParam(':sasia', $this->sasia, PDO::PARAM_INT);
        $query->bindParam(':cmimi', $this->cmimi, PDO::PARAM_INT);
        $query->bindParam(':shuma', $this->shuma, PDO::PARAM_INT);

        $query->execute();
    }

    /**
     * @return mixed
     */
    public function getKlientId()
    {
        return $this->klient_id;
    }

    /**
     * @param int $shuma
     */
    public function setShuma($shuma)
    {
        $this->shuma = $shuma;
    }

    /**
     * @return int
     */
    public function getShuma()
    {
        return $this->shuma;
    }
}