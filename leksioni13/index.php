<?php
include_once "Klient.php";
include_once "Porosi.php";
include_once "FaturaFactory.php";
include_once "PorosiRepository.php";
include_once "DergimiFaturesStrategyFactory.php";
include_once "DergimiPrintStrategy.php";
include_once "DergimiEmailStrategy.php";

$kliente = array();

$klient1 = new Klient("Sibora", "Sulaj","email");
$klient2 = new Klient("Harry", "Potter", "print");
$klient3 = new Klient("Percy", "Jackson", "print");

array_push($kliente, $klient1);
array_push($kliente, $klient2);
array_push($kliente, $klient3);

$porosi1_k1 = new Porosi($klient1, "Makarona", 3, 65);
$porosi2_k1 = new Porosi($klient1, "Oriz", 1, 135);

$porosi1_k2 = new Porosi($klient2, "Miell", 2, 80);
$porosi2_k2 = new Porosi($klient2, "Vaj", 4, 200);

$porosi1_k3 = new Porosi($klient3, "Veze", 10, 140);
$porosi2_k3 = new Porosi($klient3, "Sallam", 1, 550);

//$klient1->insert();
//$klient2->insert();
//$klient3->insert();

//$porosi1_k1->insert();
//$porosi2_k1->insert();
//$porosi1_k2->insert();
//$porosi2_k2->insert();
//$porosi1_k3->insert();
//$porosi2_k3->insert();

?>

<html>
<body>
<form action="index.php" method="post">
    <select name="kliente">
        <option value="0"><?php echo $klient1->getEmri()." ".$klient1->getMbiemri()?></option>
        <option value="1"><?php echo $klient2->getEmri()." ".$klient2->getMbiemri()?></option>
        <option value="2"><?php echo $klient3->getEmri()." ".$klient3->getMbiemri()?></option>
    </select>
    <input type="submit" name="Zgjidh" value="Zgjidh">
</form>
</body>
</html>

<?php
if (isset($_POST["Zgjidh"])){
    $klientIndex = $_POST["kliente"];
    $klient = $kliente[$klientIndex];

    $fatura = FaturaFactory::create($klient);

    $porosite = $fatura->getPorosite($klient);

    $metodaDergeses = DergimiFaturesStrategyFactory::create($klient);

    ?>
    <html>
    <h3><?php $metodaDergeses->send(); ?></h3>
    <table border="1">
        <tr>
            <th>Artikulli</th>
            <th>Sasia</th>
            <th>Cmimi</th>
        </tr>
        <?php
        foreach ($porosite as $porosi):
        ?>
        <tr>
            <td><?php echo $porosi["Artikulli"]; ?></td>
            <td><?php echo $porosi["Sasia"]; ?></td>
            <td><?php echo $porosi["Cmimi"]; ?></td>
        </tr>
        <?php endforeach;?>
        <tr>
            <td><b>Totali</b></td>
            <td></td>
            <td><?php echo $fatura->getTotali();  ?></td>
        </tr>
    </table>
    </html>
<?php
}