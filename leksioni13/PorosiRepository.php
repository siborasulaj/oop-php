<?php

include_once "Porosi.php";
include_once "DBConnect.php";

class PorosiRepository{
    private $instance;

    public function getSipasKlientit(Klient $klient){
        $this->instance = DBConnect::getInstance();
        $klient_id = $klient->getId();

        $sql = "SELECT * FROM porosi WHERE klient_id = :klient_id";
        $query = $this->instance->getConnection()->prepare($sql) ;
        $query -> bindParam(':klient_id', $klient_id);

        $query->execute();

        $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

        return $rezultati;
    }
}