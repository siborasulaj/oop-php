<?php

include_once "DBConnect.php";

class Klient{
    private $id;
    private $emri;
    private $mbiemri;
    private $metodaPageses;

    public function __construct($emri, $mbiemri, $metodaPageses){
        $this->emri = $emri;
        $this->mbiemri = $mbiemri;
        $this->metodaPageses = $metodaPageses;
    }

    /**
     * @param mixed $emri
     */
    public function setEmri($emri)
    {
        $this->emri = $emri;
    }

    /**
     * @return mixed
     */
    public function getEmri()
    {
        return $this->emri;
    }

    /**
     * @param mixed $mbiemri
     */
    public function setMbiemri($mbiemri)
    {
        $this->mbiemri = $mbiemri;
    }

    /**
     * @return mixed
     */
    public function getMbiemri()
    {
        return $this->mbiemri;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        $this->instance = DBConnect::getInstance();

        $sql = "SELECT * FROM klient WHERE Emer = :emri AND Mbiemer = :mbiemri";
        $query = $this->instance->getConnection()->prepare($sql) ;
        $query -> bindParam(':emri', $this->emri);
        $query -> bindParam(':mbiemri', $this->mbiemri);

        $query->execute();

        $rezultati = $query -> fetchAll( PDO:: FETCH_ASSOC);

        $this->id = $rezultati[0]["id"];
        return $this->id;
    }

    /**
     * @param mixed $metodaPageses
     */
    public function setMetodaPageses($metodaPageses)
    {
        $this->metodaPageses = $metodaPageses;
    }

    /**
     * @return mixed
     */
    public function getMetodaPageses()
    {
        return $this->metodaPageses;
    }

    public function insert(){
        $this->instance = DBConnect::getInstance();

        $sql = "INSERT INTO klient (Emer, Mbiemer)
                VALUES(:emri,:mbiemri)";
        $query = $this->instance->getConnection()->prepare($sql);
        $query->bindParam(':emri',$this->emri, PDO::PARAM_STR);
        $query->bindParam(':mbiemri', $this->mbiemri, PDO::PARAM_STR);

        $query->execute();
    }
}