<?php

include_once "DBConnect.php";
include_once "Porosi.php";

class Fature{
    private $instance;
    private $klienti;
    private $totali = 0;

    public function __construct(Klient $klienti){
        $this->klienti = $klienti->getId();
    }

    /**
     * @param Klient $klienti
     */
    public function setKlienti(Klient $klienti)
    {
        $this->klienti = $klienti;
    }

    /**
     * @return Klient
     */
    public function getKlienti()
    {
        return $this->klienti;
    }

    /**
     * @return mixed
     */
    public function getTotali(){
        $this->instance = DBConnect::getInstance();

        $sql = "SELECT SUM(Shuma) AS total FROM porosi WHERE klient_id = :klient_id";
        $query = $this->instance->getConnection()->prepare($sql) ;
        $query -> bindParam(':klient_id', $this->klienti );

        $query->execute();

        if($data = $query->fetch(PDO::FETCH_ASSOC)) {
            $total = $data['total'];
        }

        $this->totali = $total;

        return $this->totali;
    }

    /**
     * @param $klient
     * @return array
     */
    public function getPorosite($klient){
        $porosi = new PorosiRepository();
        return $porosi->getSipasKlientit($klient);
    }
}