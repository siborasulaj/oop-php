<?php

class DergimiFaturesStrategyFactory{
    public static function create(Klient $klient) {
        switch($klient->getMetodaPageses()){
            case "email":
                $strategy = new DergimiEmailStrategy();
                break;
            case "print":
                $strategy = new DergimiPrintStrategy ();
                break;
        }
        return $strategy;
    }
}
